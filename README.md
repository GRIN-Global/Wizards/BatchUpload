Batch Upload Wizard
=================
The Batch Upload wizard was developed by International Potato Center (CIP) and using Apache 2.0 licence.

Source code from: https://gitlab.com/CIP-Development/GRIN-Global_Client

Quick training video: https://www.youtube.com/watch?v=he1ykSadOnw

For support (training, improvements or fixs) contact to: e.rojas@cgiar.org